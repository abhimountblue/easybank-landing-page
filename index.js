const navKeyMobile = document.querySelector('.mobile-nav')
const closeKey = document.querySelector('.closeImg')
navKeyMobile.addEventListener('click', () => {
    const navKeyMobile = document.querySelector('.mobile-nav')
    const hamberger = document.querySelector('#navigations-hamberger')
    const closeKey = document.querySelector('.closeImg')
    navKeyMobile.style.display = "none"
    hamberger.style.display = 'flex'
    closeKey.style.display = 'block'
})
closeKey.addEventListener('click', () => {
    const navKeyMobile = document.querySelector('.mobile-nav')
    const hamberger = document.querySelector('#navigations-hamberger')
    const closeKey = document.querySelector('.closeImg')
    navKeyMobile.style.display = "block"
    hamberger.style.display = 'none'
    closeKey.style.display = 'none'
})
window.addEventListener("resize", (event) => {
    if (window.innerWidth > '480') {
        const navKeyMobile = document.querySelector('.mobile-nav')
        const hamberger = document.querySelector('#navigations-hamberger')
        const closeKey = document.querySelector('.closeImg')
        navKeyMobile.style.display = "none"
        hamberger.style.display = 'none'
        closeKey.style.display = 'none'
    } else {
        const navKeyMobile = document.querySelector('.mobile-nav')
        navKeyMobile.style.display = "block"
    }
})

